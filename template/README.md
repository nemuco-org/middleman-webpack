# Middleman + Webpack base template

This is a base middleman + webpack template for Otwarte Klatki.

Javascript and CSS live under [assets](assets). The javascript files form the entrypoint. Include whatever CSS you need from there and it will be extracted into the built site with the same name as the entypoint file you used.

Images are not managed via Webpack.

## Get this going

```sh
$ bundle install
$ yarn install
$ middleman start
```

## Get this going (docker)

```sh
$ make setup
$ make start
```

## Add/Removing JS Entry Points

Edit the `entry` section of [webpack.config.js](webpack.config.js) as necessary.

## CSS

This uses SASS and PostCSS but you can alter those loaders to fit your taste.
